package com.novatronic.security.firmadigital.service.impl;

import com.novatronic.security.firmadigital.exception.FirmaDigitalException;
import com.novatronic.security.firmadigital.service.CriptografiaService;
import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Test;

import javax.security.auth.x500.X500Principal;
import java.io.IOException;
import java.io.InputStream;
import java.security.cert.CertPath;
import java.security.cert.X509Certificate;
import java.util.List;

public class CustomCriptografiaServiceTest {

    @Test
    public void testObtenerCertificadosCertificadoBienFormadoExitoso() throws IOException, FirmaDigitalException {
        CriptografiaService criptografiaService = new CustomCriptografiaService();

        InputStream certificadoInputStream = CustomCriptografiaServiceTest.class.getResourceAsStream("/criptografia/certificado-attlassian.pem");
        byte[] bytesCertificado = IOUtils.toByteArray(certificadoInputStream);
        List<X509Certificate> certificados = criptografiaService.obtenerCadenaCertificados(bytesCertificado);
        Assert.assertEquals(1, certificados.size());
    }
}
