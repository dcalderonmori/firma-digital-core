package com.novatronic.security.firmadigital.service.impl;

import com.novatronic.security.firmadigital.bean.PaqueteLlaves;
import com.novatronic.security.firmadigital.exception.FirmaDigitalException;
import com.novatronic.security.firmadigital.service.AlmacenService;
import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;

public class IOFEAlmacenServiceTest {

    @Test
    public void testImportarLlavesPKCSBienFormadoExitoso() throws IOException, FirmaDigitalException {

        InputStream archivo = IOFEAlmacenServiceTest.class.getResourceAsStream("/criptografia/store-ssl.pkcs12");
        byte[] archivoBytes = IOUtils.toByteArray(archivo);

        AlmacenService almacenService = new IOFEAlmacenService();
        PaqueteLlaves paqueteLlaves = almacenService.importarLlaves(archivoBytes, "prueba", "1234");

        Assert.assertNotNull(paqueteLlaves.getLlavePrivada());
        Assert.assertNotNull(paqueteLlaves.getCadenaCertificados());
    }
}
