package test;

import com.novatronic.security.firmadigital.bean.PaqueteLlaves;
import com.novatronic.security.firmadigital.exception.FirmaDigitalException;
import com.novatronic.security.firmadigital.service.AlmacenService;
import com.novatronic.security.firmadigital.service.FirmaDigitalService;
import com.novatronic.security.firmadigital.service.impl.IOFEAlmacenService;
import com.novatronic.security.firmadigital.service.impl.IOFEAlmacenServiceTest;
import com.novatronic.security.firmadigital.service.impl.SoftwareFirmaDigitalService;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.security.PrivateKey;

/**
 * Created by dcalderon on 2/20/17.
 */
public class FirmadoAlmacen {

    public static void main(String[] args) throws IOException, FirmaDigitalException {
        PaqueteLlaves paqueteLlaves = getPaqueteLlaves();

        InputStream archivo = Firmado.class.getResourceAsStream("/archivos/archivoAFirmar.txt");
        byte[] archivoBytes = IOUtils.toByteArray(archivo);
        FirmaDigitalService firmaDigitalService = new SoftwareFirmaDigitalService();
        byte[] firmaDigitalBytes = firmaDigitalService.firmar(archivoBytes, (PrivateKey) paqueteLlaves.getLlavePrivada(), "SHA1withRSA");
        if (firmaDigitalService.comprobarFirma(archivoBytes, firmaDigitalBytes, paqueteLlaves.getCadenaCertificados()[0], "SHA1withRSA")) {
            System.out.println("Ok");
        } else {
            System.out.println("Fail");
        }
    }

    private static PaqueteLlaves getPaqueteLlaves() throws IOException, FirmaDigitalException {
        InputStream archivo = IOFEAlmacenServiceTest.class.getResourceAsStream("/criptografia/store-ssl.pkcs12");
        byte[] archivoBytes = IOUtils.toByteArray(archivo);

        AlmacenService almacenService = new IOFEAlmacenService();
        return almacenService.importarLlaves(archivoBytes, "prueba", "1234");
    }
}
