package test;

import com.novatronic.security.firmadigital.exception.FirmaDigitalException;
import com.novatronic.security.firmadigital.service.CriptografiaService;
import com.novatronic.security.firmadigital.service.impl.CustomCriptografiaService;
import com.novatronic.security.firmadigital.service.FirmaDigitalService;
import com.novatronic.security.firmadigital.service.impl.SoftwareFirmaDigitalService;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.security.PrivateKey;
import java.security.cert.CertPath;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dcalderon on 4/20/16.
 * La contraseña de la llave privada es 1234
 */
public class Firmado {

    public static void main(String[] args) throws FirmaDigitalException, IOException {

        InputStream archivo = Firmado.class.getResourceAsStream("/archivos/archivoAFirmar.txt");
        byte[] archivoBytes = IOUtils.toByteArray(archivo);
        byte[] firmaDigitalBytes = firmarArchivo(archivoBytes);
        if (comprobarFirma(archivoBytes, firmaDigitalBytes)) {
            System.out.print("Ok");
        } else {
            System.out.print("Fail");
        };
    }

    private static byte[] firmarArchivo(byte[] archivoAFirmarBytes) throws IOException, FirmaDigitalException {

        CriptografiaService criptografiaService = new CustomCriptografiaService();
        InputStream llavePrivadaInputStream = Firmado.class.getResourceAsStream("/criptografia/privado-ssl-pkcs8-der.pem");
        byte[] bytesLlavePrivada = IOUtils.toByteArray(llavePrivadaInputStream);
        PrivateKey llavePrivada = criptografiaService.obtenerLlavePrivada(bytesLlavePrivada);

        FirmaDigitalService firmaDigitalService = new SoftwareFirmaDigitalService();
        return firmaDigitalService.firmar(archivoAFirmarBytes, llavePrivada, "SHA1withRSA");
    }

    private static boolean comprobarFirma(byte[] archivo, byte[] firma) throws IOException, FirmaDigitalException {

        CriptografiaService criptografiaService = new CustomCriptografiaService();
        InputStream certificadoInputStream = Firmado.class.getResourceAsStream("/criptografia/certificado-ssl.pem");
        List<? extends Certificate> certificados = criptografiaService.obtenerCadenaCertificados(IOUtils.toByteArray(certificadoInputStream));

        FirmaDigitalService firmaDigitalService = new SoftwareFirmaDigitalService();
        return firmaDigitalService.comprobarFirma(archivo, firma, certificados.get(0), "SHA1withRSA");
    }
}
