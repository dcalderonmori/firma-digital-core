package com.novatronic.security.firmadigital.service.impl;

import com.novatronic.security.firmadigital.exception.FirmaDigitalException;
import com.novatronic.security.firmadigital.service.FirmaDigitalService;
import com.novatronic.security.firmadigital.utils.FirmaDigitalUtils;

import java.security.*;
import java.security.cert.Certificate;

public class SoftwareFirmaDigitalService implements FirmaDigitalService {

    public byte[] firmar(byte[] data, PrivateKey privateKey, String algoritmo) throws FirmaDigitalException {
        try {
            Signature signature = Signature.getInstance(algoritmo);
            signature.initSign(privateKey);
            signature.update(data);
            return signature.sign();
        } catch (NoSuchAlgorithmException e) {
            throw FirmaDigitalUtils.generarException("No existe el algoritmo especificado", e);
        } catch (InvalidKeyException e) {
            throw FirmaDigitalUtils.generarException("La llave privada especificada no es valida", e);
        } catch (SignatureException e) {
            throw FirmaDigitalUtils.generarException("Sucedio un error al firmar data", e);
        }
    }

    public boolean comprobarFirma(byte[] data, byte[] firma, Certificate certificate, String algoritmo) throws FirmaDigitalException {
        try {
            Signature signature = Signature.getInstance(algoritmo);
            signature.initVerify(certificate);
            signature.update(data);
            return signature.verify(firma);
        } catch (NoSuchAlgorithmException e) {
            throw FirmaDigitalUtils.generarException("No existe el algoritmo especificado", e);
        } catch (InvalidKeyException e) {
            throw FirmaDigitalUtils.generarException("La llave publica especificada no es valida", e);
        } catch (SignatureException e) {
            throw FirmaDigitalUtils.generarException("Sucedio un error al verificar firma", e);
        }
    }
}
