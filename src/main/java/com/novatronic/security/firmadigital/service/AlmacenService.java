package com.novatronic.security.firmadigital.service;

import com.novatronic.security.firmadigital.bean.PaqueteLlaves;
import com.novatronic.security.firmadigital.exception.FirmaDigitalException;

public interface AlmacenService {

    PaqueteLlaves importarLlaves(byte[] almacenBytes, String alias, String password) throws FirmaDigitalException;
}
