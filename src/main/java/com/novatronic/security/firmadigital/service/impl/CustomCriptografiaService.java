package com.novatronic.security.firmadigital.service.impl;

import com.novatronic.security.firmadigital.exception.FirmaDigitalException;
import com.novatronic.security.firmadigital.service.CriptografiaService;
import com.novatronic.security.firmadigital.utils.FirmaDigitalUtils;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.cert.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.List;

public class CustomCriptografiaService implements CriptografiaService {

    public PrivateKey obtenerLlavePrivada(byte[] bytesLlavePrivada) throws FirmaDigitalException {
        try {
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            KeySpec privateKeySpec = new PKCS8EncodedKeySpec(bytesLlavePrivada);
            return keyFactory.generatePrivate(privateKeySpec);
        } catch (NoSuchAlgorithmException e) {
            throw FirmaDigitalUtils.generarException("No existe el algoritmo especificado", e);
        } catch (InvalidKeySpecException e) {
            throw FirmaDigitalUtils.generarException("Sucedio un error al obtener llave privada", e);
        }
    }

    public List<X509Certificate> obtenerCadenaCertificados(byte[] bytesCertificado) throws FirmaDigitalException {
        try {
            InputStream certificadoInputStream = new ByteArrayInputStream(bytesCertificado);
            CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509");
            return (List<X509Certificate>) certificateFactory.generateCertificates(certificadoInputStream);
        } catch (CertificateException e) {
            throw FirmaDigitalUtils.generarException("Sucedio un error al obtener certificado", e);
        }
    }
}
