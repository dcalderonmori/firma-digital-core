package com.novatronic.security.firmadigital.service;

import com.novatronic.security.firmadigital.exception.FirmaDigitalException;

import java.security.cert.X509Certificate;
import java.util.Date;

public interface ValidadorCertificado {

    void validarFecha(X509Certificate certificado, Date fecha) throws FirmaDigitalException;

    void validarUsoCertificado(X509Certificate certificado) throws FirmaDigitalException;

    void validarFirmaCertificado(X509Certificate certificadoSujeto, X509Certificate certificadoEmisor) throws FirmaDigitalException;

}
