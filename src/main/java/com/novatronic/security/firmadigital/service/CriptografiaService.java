package com.novatronic.security.firmadigital.service;

import com.novatronic.security.firmadigital.exception.FirmaDigitalException;

import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import java.util.List;

/**
 * Created by dcalderon on 4/23/16.
 */
public interface CriptografiaService {

    PrivateKey obtenerLlavePrivada(byte[] bytesLlavePrivada) throws FirmaDigitalException;

    List<X509Certificate> obtenerCadenaCertificados(byte[] bytesCertificado) throws FirmaDigitalException;

}
