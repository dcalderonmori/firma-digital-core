package com.novatronic.security.firmadigital.service.impl;

import com.novatronic.security.firmadigital.exception.FirmaDigitalException;
import com.novatronic.security.firmadigital.service.FirmaDigitalService;
import com.novatronic.security.firmadigital.service.ValidadorCertificado;
import com.novatronic.security.firmadigital.utils.FirmaDigitalUtils;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SignatureException;
import java.security.cert.*;
import java.util.Date;
import java.util.List;

public class IOFEValidadorCertificado implements ValidadorCertificado {

    private static final int POSICION_FIRMA_DIGITAL = 0;
    private FirmaDigitalService firmaDigitalService;

    public void validarFecha(X509Certificate certificado, Date fecha) throws FirmaDigitalException {
        try {
            certificado.checkValidity(fecha);
        } catch (CertificateExpiredException e) {
            throw FirmaDigitalUtils.generarException("El certificado esta expirado", e);
        } catch (CertificateNotYetValidException e) {
            throw FirmaDigitalUtils.generarException("El certificado aun no es valido", e);
        }
    }

    public void validarUsoCertificado(X509Certificate certificado) throws FirmaDigitalException {
        boolean soportaFirmaDigital = certificado.getKeyUsage()[POSICION_FIRMA_DIGITAL];
        if (!soportaFirmaDigital) {
            throw FirmaDigitalUtils.generarException("El certificado no soporta la operacion de firma");
        }
    }

    public void validarFirmaCertificado(X509Certificate certificadoSujeto, X509Certificate certificadoEmisor) throws FirmaDigitalException {
        try {
            certificadoSujeto.verify(certificadoEmisor.getPublicKey());
        } catch (CertificateException e) {
            throw FirmaDigitalUtils.generarException("El certificado no soporta la operacion de firma", e);
        } catch (NoSuchAlgorithmException e) {
            throw FirmaDigitalUtils.generarException("El certificado no soporta la operacion de firma", e);
        } catch (InvalidKeyException e) {
            throw FirmaDigitalUtils.generarException("El certificado no soporta la operacion de firma", e);
        } catch (NoSuchProviderException e) {
            throw FirmaDigitalUtils.generarException("El certificado no soporta la operacion de firma", e);
        } catch (SignatureException e) {
            throw FirmaDigitalUtils.generarException("El certificado no soporta la operacion de firma", e);
        }
    }
}
