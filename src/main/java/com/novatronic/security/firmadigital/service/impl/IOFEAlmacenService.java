package com.novatronic.security.firmadigital.service.impl;

import com.novatronic.security.firmadigital.bean.PaqueteLlaves;
import com.novatronic.security.firmadigital.exception.FirmaDigitalException;
import com.novatronic.security.firmadigital.service.AlmacenService;
import com.novatronic.security.firmadigital.utils.FirmaDigitalUtils;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.security.*;
import java.security.cert.*;
import java.security.cert.Certificate;

public class IOFEAlmacenService implements AlmacenService {

    public PaqueteLlaves importarLlaves(byte[] almacenBytes, String alias, String password) throws FirmaDigitalException {
        try {
            KeyStore keyStore = KeyStore.getInstance("pkcs12");
            keyStore.load(new ByteArrayInputStream(almacenBytes), password.toCharArray());
            Key llavePrivada = keyStore.getKey(alias, password.toCharArray());
            Certificate[] cadenaCertificados = keyStore.getCertificateChain(alias);

            return new PaqueteLlaves(llavePrivada, cadenaCertificados);
        } catch (KeyStoreException e) {
            throw FirmaDigitalUtils.generarException("Formato de almacen no encontrado", e);
        } catch (CertificateException e) {
            throw FirmaDigitalUtils.generarException("Excepcion al obtener certificado", e);
        } catch (NoSuchAlgorithmException e) {
            throw FirmaDigitalUtils.generarException("No existe el algoritmo especificado en el almacen", e);
        } catch (IOException e) {
            throw FirmaDigitalUtils.generarException("Sucedio un error inesperado", e);
        } catch (UnrecoverableKeyException e) {
            throw FirmaDigitalUtils.generarException("No se logro obtener la llave privada", e);
        }
    }
}
