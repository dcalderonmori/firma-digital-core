package com.novatronic.security.firmadigital.service;

import com.novatronic.security.firmadigital.exception.FirmaDigitalException;

import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.util.Collection;

public interface FirmaDigitalService {

    byte[] firmar(byte[] data, PrivateKey privateKey, String algoritmo) throws FirmaDigitalException;

    boolean comprobarFirma(byte[] data, byte[] firma, Certificate certificate, String algoritmo) throws FirmaDigitalException;
}
