package com.novatronic.security.firmadigital.exception;

public class FirmaDigitalException extends Exception {

    public FirmaDigitalException(String mensaje) {
        super(mensaje);
    }

    public FirmaDigitalException(String message, Throwable cause) {
        super(message, cause);
    }
}
