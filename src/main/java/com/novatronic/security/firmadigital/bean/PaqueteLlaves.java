package com.novatronic.security.firmadigital.bean;

import java.security.Key;
import java.security.cert.Certificate;

public class PaqueteLlaves {

    private Key llavePrivada;
    private Certificate[] cadenaCertificados;

    public PaqueteLlaves(Key llavePrivada, Certificate[] cadenaCertificados) {
        this.llavePrivada = llavePrivada;
        this.cadenaCertificados = cadenaCertificados;
    }

    public Key getLlavePrivada() {
        return llavePrivada;
    }

    public Certificate[] getCadenaCertificados() {
        return cadenaCertificados;
    }
}
