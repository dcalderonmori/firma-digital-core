package com.novatronic.security.firmadigital.utils;

import com.novatronic.security.firmadigital.exception.FirmaDigitalException;

public class FirmaDigitalUtils {

    public static FirmaDigitalException generarException(String mensaje) {
        return new FirmaDigitalException(mensaje);
    }

    public static FirmaDigitalException generarException(String mensaje, Throwable e) {
        e.printStackTrace();
        return new FirmaDigitalException(mensaje, e);
    }
}
